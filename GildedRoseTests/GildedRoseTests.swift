//
//  GildedRoseTests.swift
//  GildedRoseTests
//
//  Created by Matt Jacquet on 7/23/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import XCTest
@testable import GildedRose


class GildedRoseTests: XCTestCase {

    func testDexterityVestSellInDecrease() {
        let sellInValue = 10
        let items = [Item(name: ItemType.dexterityVest.rawValue, sellIn: sellInValue, quality: 20)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(sellInValue - 1, app.viewModelItems[0].sellIn);
    }
    
    func testAgedBriesSellInDecrease() {
        let sellInValue = 0
        let items = [Item(name: ItemType.agedBrie.rawValue, sellIn: sellInValue, quality: 20)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(sellInValue - 1, app.viewModelItems[0].sellIn);
    }
    
    func testSulfurasfterExpiringDate() { // check what I wanted to test cause it was elixir in title and sulfuras item
        let items = [Item(name: ItemType.sulfuras.rawValue, sellIn: 0, quality: 20)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 80);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 80);
    }
    
    func testBackstagePassesNonNegativeQuality() {
        let items = [Item(name: ItemType.backstagePasses.rawValue, sellIn: 0, quality: 2)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 0);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 0);
    }
    
    func testAgedBrieMax50Quality() {
        let items = [Item(name: ItemType.agedBrie.rawValue, sellIn: 5, quality: 50)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 50);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 50);
    }
    
    func testAgedBrieIncreasingQuality() {
        let items = [Item(name: ItemType.agedBrie.rawValue, sellIn: 5, quality: 15)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 16);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 17);
    }
    
    func testBackstagePassesIncreasingQualityWhen10OrLessDaysLeft() {
        let items = [Item(name: ItemType.backstagePasses.rawValue, sellIn: 10, quality: 15)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 18);
    }
    
    func testBackstagePassesIncreasingQualityWhen5OrLEssDaysLeft() {
        let items = [Item(name: ItemType.backstagePasses.rawValue, sellIn: 5, quality: 15)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 18);
    }
    
    func testBackstagePassesQualityTo0WhenConcertDone() {
        let items = [Item(name: ItemType.backstagePasses.rawValue, sellIn: 0, quality: 15)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 0);
    }
    
    func testSulfuraQualityNotChangingWithTime() {
        let items = [Item(name: ItemType.sulfuras.rawValue, sellIn: 20, quality: 80)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 80);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 80);
    }
    
    func testConjuredQualityDecreasing2TimesMoreThanOtherItems() {
        let items = [Item(name: ItemType.conjuredManaCake.rawValue, sellIn: 20, quality: 15)]
        let app = ItemViewModel(items: items);
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 13);
        
        app.updateItems()
        XCTAssertEqual(app.viewModelItems[0].quality, 11);
    }
}
