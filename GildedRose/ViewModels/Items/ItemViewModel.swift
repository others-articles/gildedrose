//
//  ItemViewModel.swift
//  GildedRose
//
//  Created by Matt Jacquet on 7/29/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import Foundation

class ItemViewModel: NSObject {
    
   public var viewModelItems = [ViewModelItem]()
    
    init(items: [Item]) {
       super.init()
        
        for item in items {
            
            var currentItem: ViewModelItem!
        
            switch ItemType(rawValue: item.name) {
            case .agedBrie:
                currentItem = AgedBrieManager(item: item)
                viewModelItems.append(currentItem)
            case .backstagePasses:
                currentItem = BackstagePassesManager(item: item)
                viewModelItems.append(currentItem)
            case .conjuredManaCake:
                currentItem = ConjuredManaCakeManager(item: item)
                viewModelItems.append(currentItem)
            case .dexterityVest:
                currentItem = DexterityVestManager(item: item)
                viewModelItems.append(currentItem)
            case .elixirMongoose:
                currentItem = elixirMongooseManager(item: item)
                viewModelItems.append(currentItem)
            case .sulfuras:
                currentItem = SulfurasManager(item: item)
                viewModelItems.append(currentItem)
            default:
                break
            }
        }
    }
    
    public func updateItems() {
         for item in viewModelItems {
             item.updateQuality()
             item.updateSellIn()
         }
     }
}
