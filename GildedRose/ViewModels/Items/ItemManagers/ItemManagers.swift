//
//  ItemQualityManager.swift
//  GildedRose
//
//  Created by Matt Jacquet on 7/27/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

import Foundation

protocol ViewModelItem {
    var quality: Int { get set }
    var sellIn: Int { get set }
    var name: ItemType { get set }
    func updateQuality()
    func updateSellIn()
}

class AgedBrieManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .agedBrie
    }
    
    func updateQuality() {
        if quality < ItemConst.MAX_QUALITY {
            quality += 1
        }
    }
    
    func updateSellIn() {
        sellIn -= 1
    }
}

class BackstagePassesManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .backstagePasses
    }
    
    func updateQuality() {
        if sellIn <= 0 {
            quality = 0
        }else if sellIn <= 3 {
            quality += 2
        }else if sellIn <= 10 {
            quality += 3
        }else {
            quality += 1
        }
        
        if quality > ItemConst.MAX_QUALITY {
            quality = ItemConst.MAX_QUALITY
        }
    }
    
    func updateSellIn() {
        sellIn = sellIn - 1
    }
}

class ConjuredManaCakeManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .conjuredManaCake
    }
    
    func updateQuality() {
        quality -= 2
        
        
        if quality < ItemConst.MIN_QUALITY {
            quality = ItemConst.MIN_QUALITY
        }
    }
    
    func updateSellIn() {
        sellIn -= 1
    }
}

class elixirMongooseManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .agedBrie
    }
    
    func updateQuality() {
        quality -= 1
    }
    
    func updateSellIn() {
        sellIn -= 1
    }
}

class DexterityVestManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .agedBrie
    }
    
    func updateQuality() {
        quality -= 1
        
    }
    
    func updateSellIn() {
        sellIn -= 1
    }
}

class SulfurasManager: ViewModelItem {
    
    var quality: Int
    var sellIn: Int
    var name: ItemType
    
    init(item: Item) {
        self.quality = item.quality
        self.sellIn = item.sellIn
        self.name = .agedBrie
    }
    
    func updateQuality() {
        quality = ItemConst.LEGENDARY_ITEM_QUALITY
    }
    
    func updateSellIn() { }
}





















