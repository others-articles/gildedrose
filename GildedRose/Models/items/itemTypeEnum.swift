//
//  itemType.swift
//  GildedRose
//
//  Created by Matt Jacquet on 7/24/20.
//  Copyright © 2020 Matt Jacquet - Appro. All rights reserved.
//

enum ItemType: String {
    case agedBrie = "Aged brie"
    case dexterityVest = "+5 Dexterity Vest"
    case elixirMongoose = "Elixir of the Mongoose"
    case sulfuras = "Sulfuras, Hand of Ragnaros"
    case backstagePasses = "Backstage passes to a TAFKAL80ETC concert"
    case conjuredManaCake = "Conjured Mana Cake"
}

