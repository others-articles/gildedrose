public struct Item {
    public var name: String
    public var sellIn: Int
    public var quality: Int
    
    public init(name: String, sellIn: Int, quality: Int) {
        self.name = name
        self.sellIn = sellIn
        self.quality = quality
    }
}

extension Item: CustomStringConvertible {
    public var description: String {
        return self.name + ", " + String(self.sellIn) + ", " + String(self.quality);
    }
}

struct ItemConst {
    static let MAX_QUALITY = 50
    static let MIN_QUALITY = 0
    static let LEGENDARY_ITEM_QUALITY = 80
}
